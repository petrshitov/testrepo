public class ThirdTask {

    public static void main(String[] args) {
        System.out.println(factorial(20));
    }

    private static long factorial(long f) {
        long result;

        if (f == 1)
            return 1;
        result = factorial(f - 1) * f;

        return result;
    }
}