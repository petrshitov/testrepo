import java.io.*;
import java.util.*;

public class SecondTask {

    public static void main(String[] args) throws IOException {
        createFile();
        sortingFromLowToHigh();
        sortingFromHighToLow();
    }

    private static void createFile() throws IOException {

        FileWriter fileWriter = new FileWriter("d:\\test.txt");

        ArrayList arrayList = new ArrayList();

        for (int i = 0; i <= 20; i++) {
            arrayList.add(i);
        }

        Collections.shuffle(arrayList);

        for (int i = 0; i <= 20; i++) {
            fileWriter.write(arrayList.get(i).toString());
            fileWriter.write(",");
        }

        fileWriter.close();
    }

    private static void sortingFromLowToHigh() throws IOException {

        FileReader fileReader = new FileReader("d:\\test.txt");
        Scanner scanner = new Scanner(fileReader).useDelimiter(",");
        ArrayList<Integer> list = new ArrayList<>();

        while (scanner.hasNext()) {
            list.add(scanner.nextInt());
        }

        fileReader.close();

        Collections.sort(list);

        for (int s:list) {
            System.out.print(s);
            System.out.print(",");
        }

        System.out.println();
    }

    private static void sortingFromHighToLow() throws IOException {

        FileReader fileReader = new FileReader("d:\\test.txt");
        Scanner scanner = new Scanner(fileReader).useDelimiter(",");
        ArrayList<Integer> list = new ArrayList<>();

        while (scanner.hasNext()) {
            list.add(scanner.nextInt());
        }

        fileReader.close();

        Collections.sort(list, Collections.reverseOrder());

        for (int s:list) {
            System.out.print(s);
            System.out.print(",");
        }
    }



    }

